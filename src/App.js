import React, {Component} from 'react';
import './App.css';
import Numbers from './Numbers/Numbers';

class App extends Component {
    state = {
        numbers: []
    };

    generateRandomNumbers() {
        let randomNumbersArray = [];

        for (let i = 0; randomNumbersArray.length < 5; i++) {
            let arrayItem = Math.floor(5 + Math.random() * (36 + 1 - 5));
            let checkItem = true;


            randomNumbersArray.forEach(function (index) {
                if (index === arrayItem) {
                    checkItem = false;
                }
            });
            
            if (checkItem) {
                randomNumbersArray.push(arrayItem);
            }

        }

        function checkNumbers(a, b) {
            return a - b;
        }

        randomNumbersArray.sort(checkNumbers);

        this.setState({numbers: randomNumbersArray});
    }

    changeNumbers = () => {
        this.generateRandomNumbers();
    };

    componentDidMount() {
        this.generateRandomNumbers()
    }

    render() {
        return (
            <div className="App">
                <div>
                    <button onClick={this.changeNumbers}>New numbers</button>
                </div>
                <Numbers number1={this.state.numbers[0]} number2={this.state.numbers[1]} number3={this.state.numbers[2]}
                         number4={this.state.numbers[3]} number5={this.state.numbers[4]}/>
            </div>
        );
    }

}

export default App;
