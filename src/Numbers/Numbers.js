import React from 'react';

const Numbers = (props) => {
    return (
        <div className="numbers">
            <span className="number">{props.number1}</span>
            <span className="number">{props.number2}</span>
            <span className="number">{props.number3}</span>
            <span className="number">{props.number4}</span>
            <span className="number">{props.number5}</span>
        </div>
    );
};

export default Numbers;
